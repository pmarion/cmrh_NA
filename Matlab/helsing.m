function A = helsing(n)
%
%function [A] = helsing(n)
% 
% This file creates a matrix A whose entries are defined by :
% A(i,j)= -log|z(i)-z(j)| , if i .neq. j
%         -log|r(i)|
% where z(i) are n somehow randomly distributed points in a unit square
% centered at the origin in the complex plane and where each r(i) is a 
% number in (0,d(i)[, d(i) being the distance between the point z(i) and 
% its nearest neighbour. 

%
% Input : 
%           n       : size of the matrix A
% Output : 
%           A       : square matrix of size n as described above 
%
% 07 February 13
%
xi=rand(n,1)*2-1;
yi=rand(n,1)*2-1;
[xj,yj]=meshgrid(xi,yi);
xj=xj'-xj;
yj=yj'-yj;
coef=-1;
for i=1:max(size(xj))
    d0=0;
    for j=1:n
        temp=sqrt((xi(i)-xi(j))^2+(yi(j)-yi(i))^2);
        if (temp>0)
           A(i,j)=coef*log(temp);
           if (d0==0)
               d0=j;
               A(i,i)=-log(0.5*d0*rand());
           end
        end
    end
end