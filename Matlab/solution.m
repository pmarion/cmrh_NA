function [b,sol]=solution(A,i_rhs)
%
% function [b,sol] = solution(A,i_rhs)
%
% This function allows to generate a right hand side b for
% the linear system A*x=b so as that the exact solution is known.
%
% Input :   
%           A       : the coefficient matrix of the linear system A*x=b
%           i_rhs   : integer (i_rhs = 1 or 2 or 3)
% if i_rhs = 1 then the entries of the exact solution of the linear system 
% are generated randomly in [0, 1]
% if i_rhs = 2 then the entries of the exact solution of the linear 
% system equals 1.
% if i_rhs = 3 then the k-th entry of the exact solution of the linear 
% system equals k.
%
% Output :  
%           b       : the right hand side of the linear system A*x=b
%           sol     : the exact solution of the linear system A*x=b
%
n = length(A)
    
if (i_rhs==1)
   sol=rand(n,1);
end
if (i_rhs==2)
   sol=ones(n,1);
end
if (i_rhs==3)
   sol=[1:n]';
end
b=A*sol;