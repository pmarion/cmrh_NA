function A=matrice(n,i_mat)
% 
% function [A] = matrice(n,i_mat)
% 
% This function allows to choose the coefficient matrix of
% the linear system A*x=b.  
% 
% Input :   
%           n       : the size of the coefficient matrix of 
%                     the linear system A*x=b
%           i_mat   : integer (i_rhs = 1 or 2 or 3 or 4)
% if i_mat = 1 then A(i,j) = (alpha*j-beta) / (n-i+j)  if j <= i 
%              and  A(i,j) = (alpha*i-beta) / (n-i+j)  if j > i   
% if i_mat = 2 then A(i,j) = i if j <= i   
%              and  A(i,j) = j if j > i 
% if i_mat = 3 then A(i,j) = abs(i-j)+1/(i-j) if (j < i  and j > i)
%              and  A(i,j) = 0  if i=j   \n')
% if i_mat = 4 then A(i,j)= i+j
% 
% Output :  
%           A       : the coefficient matrix of the linear system A*x=b
% 
% 
if (i_mat==1)
    a=input('Enter alpha : ');
    b=input('Enter beta : ');
    for i=1:n
        for j=1:i
            A(i,j)=(a*j-b)/(n-i+j);
        end
        for j=i+1:n
            A(i,j)=(a*i-b)/(n-i+j);
        end
    end
end
if (i_mat==2)
   for i=1:n
       for j=1:i
           A(i,j)=i;
       end
       for j=i+1:n
           A(i,j)=j;
       end
   end
end
if (i_mat==3)
   for i=1:n
       for j=1:i-1
           A(i,j)=abs(i-j)+1/(i-j);
       end
       for j=i+1:n
           A(i,j)=abs(i-j)+1/(i-j);
       end
       A(i,i)=0;
   end
end
if (i_mat==4)
   for i=1:n
       for j=1:n
           A(i,j)=i+j;
       end
   end
end