program vecrand
! vecrand.f90 
! This file creates two random vectors and stores them in the file vecrand.dat
!
! 
 
  implicit none
  integer n,i,j,a,b,init(4)
  real(kind=8),allocatable,dimension(:) ::x,y
  integer*4,dimension(3)  ::timearray
  real                     ::rand
  call itime(timearray)     ! Get the current time
  i = rand ( timearray(1)+timearray(2)+timearray(3) )

open(unit=20,file='inputfile.dat',status='old')
read(20,*)init
n=init(1)
  a=1
  b=0
  do while(a.ge.b)
     print*,'Enter the interval limits : '
     read*,a,b
     if (a.ge.b) then
        print*,'Choose a<b'
     endif
  end do
  allocate(x(n),y(n))
  do i=1,n
     x(i)=rand(0)*dble(b-a)-dble(b);
     y(i)=rand(0)*dble(b-a)-dble(b);
  enddo
  open(unit=10,file='vecrand.dat',status='unknown')
  do i=1,n
     write(unit=10,FMT=*) x(i),y(i)
  enddo
  close(10)
  deallocate(x,y)
end program vecrand
