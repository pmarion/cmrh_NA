program initialisation
! This program creates the file inputfile.dat which is used in 
! CMRH (sequential) or PCMRH (parallel) test suite.
!
! - output choice (no prints/ all iterations & results / only final results) 
! - size of matrix A.
! - choice between 5 types of matrices A
! - choice between 3 types of solution vectors
! - tolerance input.

use randvectors
integer :: iprint,n,choix,choixsol
real*8 ::choixtol
print*,'Would you print results ? : 1-No / 2-All / 3-Time and final residual norm'
read*,iprint
print*,'Enter matrix size : '
read*,n
print*,'Enter matrix choice : (more details in README.txt) '
print*,'1- A(i,j)=2j-1 / n-i+j for j=1,k   '
print*,'          2i-1 / n-i+j for j=i+1,n '
print*,'2- A(i,j)=i for j=1,i '
print*,'          j for j=i+1,n '
print*,'3-helsing.f90'
print*,'4-rbf.f90 (Radial basis function matrix - see rbf.f90 - )'
print*,'5- Matrix File (put your matrixfile.dat in current directory) '
read*,choix
if (choix==3) then
    call rand0(n)
    call vecrand(n)
    call helsing(n)
end if
if (choix==4) then
    call rand0(n)
    call vecrand(n)
    call rbf(n)
end if


print*,'Enter solution choice : 1- [1,...,1]^T  / 2- [n,...,1]^T '
print*,'3-rand vector'
read*,choixsol

print*,'Enter tolerance:'
read*,choixtol

open(unit=10,file='inputfile.dat',status='unknown')
write(10,*) iprint,n,choix,choixsol,choixtol
close(10)
end  program initialisation
