program rand0
! rand0.f90
! This file creates a random vector of size n and stores it in rand0.dat
! Author: Sebastien Duminil

  implicit none
  integer n,i,init(4)
  real(kind=8),allocatable,dimension(:) ::x
  integer*4,dimension(3)  ::timearray
  real                     ::rand
  call itime(timearray)     ! Get the current time
  i = rand ( timearray(1)+timearray(2)+timearray(3) )
  open(unit=20,file='inputfile.dat',status='old')
  read(20,*)init
  n=init(1)
  close(20)
  allocate(x(n))
  do i=1,n
     x(i)=rand(0);
  enddo
  open(unit=10,file='rand0.dat',status='unknown')
  do i=1,n
     write(unit=10,FMT=*) x(i)
  enddo
  close(10)
  deallocate(x)
end program rand0
