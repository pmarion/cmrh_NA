  ***************************************************************************
  * All the software  contained in this library  is protected by copyright. *
  * Permission  to use, copy, modify, and  distribute this software for any *
  * purpose without fee is hereby granted, provided that this entire notice *
  * is included  in all copies  of any software which is or includes a copy *
  * or modification  of this software  and in all copies  of the supporting *
  * documentation for such software.                                        *
  ***************************************************************************
  * THIS SOFTWARE IS BEING PROVIDED "AS IS", WITHOUT ANY EXPRESS OR IMPLIED *
  * WARRANTY. IN NO EVENT, NEITHER  THE AUTHORS, NOR THE PUBLISHER, NOR ANY *
  * MEMBER  OF THE EDITORIAL BOARD OF  THE JOURNAL  "NUMERICAL ALGORITHMS", *
  * NOR ITS EDITOR-IN-CHIEF, BE  LIABLE FOR ANY ERROR  IN THE SOFTWARE, ANY *
  * MISUSE  OF IT  OR ANY DAMAGE ARISING OUT OF ITS USE. THE ENTIRE RISK OF *
  * USING THE SOFTWARE LIES WITH THE PARTY DOING SO.                        *
  ***************************************************************************
  * ANY USE  OF THE SOFTWARE  CONSTITUTES  ACCEPTANCE  OF THE TERMS  OF THE *
  * ABOVE STATEMENT.                                                        *
  ***************************************************************************

     AUTHORS:
        S. Duminil, Ph. Marion, H. Sadok
	 University of Littoral Calais, France
	 E-mail: duminil@lmpa.univ-littoral.fr, sadok@lmpa.univ-littoral.fr,
	 marion@lmpa.univ-littoral.fr

		
	Mohammed Heyouni
        University Mohammed Premier Oujda, Maroc
        E-mail: mohammed.heyouni@gmail.com

      REFERENCES:
	
	- CMRH: A new method for solving non symmetric linear systems based on the
	Hessenberg reduction algorithms, Numer. Algorithms, 20, (1999), pp 303-321
        - A new implementation of the CMRH method for solving dense linear systems, 
	  J. Comput. Appl. Math., 213 (2008), pp 387-399
	- A parallel implementation of the CMRH method for dense linear systems, 
	  Numer. Algorithms, 63 (2013), 127-142

      SOFWARE REVISION DATE:
	  v1.0, January 2013

      SOFTWARE LANGUAGE:
	  Fortran 90 and MATLAB 8.0 (R2012b)

=============================================================================
SOFTWARE
=============================================================================

This package provides a set of Fortran and Matlab functions to solve linear systems using CMRH method. 

=============================================================================
PACKAGE
=============================================================================

The main directory contains the following files

* README.txt	 :	This file

* Fortran 90 subroutines : 

-makefile	      	: to install and config all fortran programs
-inputfile.dat      	: In this file, we choose the size of system, the choice of
 			  matrix, the choice of solution and the print version
-initialisation.f90 	: This file creates inputfile.dat 
-CMRH.f90      		: CMRH program
-PCMRH.f90		: Parallel CMRH program
-helsing.f90		: Matrix test stored in matrixfile.dat
-rbf.f90		: Matrix test stored in matrixfile.dat
-rand0.f90		: Create a rand vector stored in rand0.dat
-vecrand.f90		: Create two rand vectors stored in vecrand.dat
-randvectors.f90  	: Contains some subroutines  

* Matlab programs :
-main.m  		: main program
-CMRH.m			: CMRH function
-matrice.m		: To create some matrix example
-solution.m		: To create some solution vector example
-mmread.m		: from matrix market. To store matrix market file in A
-helsing.m		: Matrix test
-gemat11.mtx		: A matrix market example


=============================================================================
HOW TO INSTALL
=============================================================================
When the archive file containing the package is uncompressed, it creates a directory named CMRHprograms. 
The directory fortran contains the fortran codes. 
The directory matlab contains the matlab codes.

* Fortran 90 programs :
- enter to fortran directory
- Open makefile
- Config fortran, mpi, blas and lapack compilers 

* Matlab programs :
- enter to matlab directory
- Run main.m

=============================================================================
HOW TO COMPILE
=============================================================================
- make all (to config all programs) or make allseq (to config only sequential programs) 
- ./initialisation  (to config inputfile.dat)
- ./CMRH  (to run CMRH program)
- mprun -n (nb of proc) ./PCMRH (to run PCMRH program)


=============================================================================
NUMERICAL TESTS
=============================================================================
FORTRAN PROGRAMS :
- Run ./initialisation to config the matrix :  
	-helsing : Matrix arising from Helsing paper (choose 3)
	-rbf   	 : Matrix arising from radial basis functions (choose 4)
	-Own matrix : save it in matrixfile.dat  (choose 5)
- Run ./CMRH or mprun -n (nb of proc) ./PCMRH 

MATLAB PROGRAMS :
- Run main.m
- Choose the matrix example.
- For matrix market test, choose 6 and enter the file name .mtx 


=============================================================================
DELETE FORTRAN FILES
=============================================================================
- make clean
